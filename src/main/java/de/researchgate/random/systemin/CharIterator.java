package de.researchgate.random.systemin;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CharIterator implements Iterator<Character> {

    private final BufferedReader bufferedReader;
    private Character cachedCharacter;
    private boolean finished = false;

    public CharIterator(final BufferedReader reader) throws IllegalArgumentException {
        if (reader == null) {
            throw new IllegalArgumentException("Reader must not be null");
        }
        bufferedReader = new BufferedReader(reader);
    }

    public boolean hasNext() {
        if (cachedCharacter != null) {
            return true;
        } else if (finished) {
            return false;
        } else {
            try {
                while (true) {
                    int line = bufferedReader.read();
                    if (line == -1) {
                        finished = true;
                        return false;
                    } else if (isValidLine((char) line)) {
                        cachedCharacter = (char) line;
                        return true;
                    }
                }
            } catch (IOException ioe) {
                close();
                throw new IllegalStateException(ioe.toString());
            }
        }
    }

    protected boolean isValidLine(Character c) {
        return true;
    }

    public Character next() {
        return nextLine();
    }

    public Character nextLine() {
        if (!hasNext()) {
            throw new NoSuchElementException("No more lines");
        }
        Character currentLine = cachedCharacter;
        cachedCharacter = null;
        return currentLine;
    }

    public void close() {
        finished = true;
        cachedCharacter = null;
    }

    public void remove() {
        throw new UnsupportedOperationException("Remove unsupported on CharIterator");
    }

}