package de.researchgate.random.secure;

import de.researchgate.random.RandomProvider;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.stream.Stream;

public class SecureRandomProvider implements RandomProvider {

    private SecureRandom random = new SecureRandom();

    private String getRandomString(int numBits) {
        return new BigInteger(numBits * 5, random).toString(32);
    }

    @Override
    public Stream<Character> getRandomCharStream(Integer max) {
        String randomString = getRandomString(max);
        printRandomString(randomString);
        return randomString.chars()
                .mapToObj(i -> (char) i);
    }
}
