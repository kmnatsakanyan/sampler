package de.researchgate.random.secure;

import de.researchgate.random.RandomProvider;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class SecureRandomProviderTest {

    @Test
    public void testGetRandomCharList() throws Exception {
        RandomProvider secureRandomProvider = new SecureRandomProvider();

        List<Character> firstRandomCharList = secureRandomProvider.getRandomCharStream(10).collect(Collectors.toList());
        assertEquals(10, firstRandomCharList.size());

        List<Character> secondRandomCharList = secureRandomProvider.getRandomCharStream(500).collect(Collectors.toList());
        assertEquals(500, secondRandomCharList.size());
    }
}