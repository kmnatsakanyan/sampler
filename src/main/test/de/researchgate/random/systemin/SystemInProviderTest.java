package de.researchgate.random.systemin;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.util.List;
import java.util.stream.Collectors;

public class SystemInProviderTest {

    @Rule
    public final TextFromStandardInputStream systemInMock
            = TextFromStandardInputStream.emptyStandardInputStream();

    @Test
    public void testGetRandomCharList() throws Exception {
        systemInMock.provideLines("foo", "bar");
        SystemInProvider systemInProvider = new SystemInProvider();
        List<Character> randomCharList = systemInProvider.getRandomCharStream(5).collect(Collectors.toList());
        String charListStr = randomCharList
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining());
        Assert.assertEquals("foo\nbar\n", charListStr);
    }
}