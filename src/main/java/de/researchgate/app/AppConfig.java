package de.researchgate.app;

import de.researchgate.random.orgrandom.PrimitiveConverterFactory;
import de.researchgate.random.orgrandom.RandomApiClient;
import retrofit2.Retrofit;

public class AppConfig {

    public static RandomApiClient getRandomApiClient() {
        return new Retrofit.Builder()
                .baseUrl("https://www.random.org/")
                .addConverterFactory(PrimitiveConverterFactory.create())
                .build()
                .create(RandomApiClient.class);
    }
}
