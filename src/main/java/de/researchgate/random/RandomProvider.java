package de.researchgate.random;

import java.util.stream.Stream;

public interface RandomProvider {

    Stream<Character> getRandomCharStream(Integer max);

    default void printRandomString(String randomString) {
        System.out.println("########## Random String ##########");
        System.out.println(randomString);
    }
}
