package de.researchgate.random.systemin;

import de.researchgate.random.RandomProvider;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SystemInProvider implements RandomProvider {

    @Override
    public Stream<Character> getRandomCharStream(Integer max) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new CharIterator(br), Spliterator.ORDERED), false);
    }


}
