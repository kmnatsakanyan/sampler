package de.researchgate.app;


import de.researchgate.random.RandomProvider;
import de.researchgate.random.orgrandom.RandomOrgProvider;
import de.researchgate.random.secure.SecureRandomProvider;
import de.researchgate.random.systemin.SystemInProvider;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpecBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommandLineTools {

    private static final int MAX_RANDOM_ORG = 20;
    private static final int KO = 1;
    private static final int OK = 0;

    private OptionParser parser;

    public CommandLineTools() {
        this.parser = getParser();
    }

    protected Optional<Sampler> tryCreateSampler(String[] args) {

        Optional<OptionSet> mayBeOptionSet = tryParse(args);
        runHelpIfAsked(mayBeOptionSet);

        Optional<RandomProvider> firstProvider = getFirstSetProvider(mayBeOptionSet);
        Optional<Integer> mayBeSampleSize = getValidatedSampleSize(mayBeOptionSet);
        Optional<Integer> mayBeRandomStringLength = getValidatedRandomStringLength(mayBeOptionSet);

        return firstProvider
                .flatMap(provider -> mayBeSampleSize
                        .flatMap(size -> mayBeRandomStringLength
                                .map(length -> new Sampler(size, length, provider))));
    }

    protected void printResult(List<Character> result) {
        String str = result.stream().map(Object::toString).collect(Collectors.joining());
        print("########## Sample ##########");
        print(str);
    }

    private Optional<Integer> getValidatedRandomStringLength(Optional<OptionSet> mayBeOptionSet) {
        Optional<Integer> mayBeRandomStringLength = getRandomStringLength(mayBeOptionSet);
        Optional<Boolean> mayBeProviderRandomOrgArg = getRandomOrgProviderArg(mayBeOptionSet);
        Optional<Boolean> validateStringLength = getValidStringLength(mayBeProviderRandomOrgArg, mayBeRandomStringLength);

        checkOptional(validateStringLength, "random.org generates max 20 length random string");

        return mayBeRandomStringLength;
    }

    private Optional<RandomProvider> getFirstSetProvider(Optional<OptionSet> mayBeOptionSet) {
        //read provider
        Optional<Boolean> mayBeProviderRandomOrgArg = getRandomOrgProviderArg(mayBeOptionSet);
        Optional<Boolean> mayBeProviderSecureRandomArg = getSecureRandomProviderArg(mayBeOptionSet);
        Optional<Boolean> mayBeProviderSystemInArg = getSystemInProviderArg(mayBeOptionSet);

        //resolve provider
        Optional<RandomProvider> mayBeProviderRandomOrg = getRandomProvider(mayBeProviderRandomOrgArg, x -> new RandomOrgProvider());
        Optional<RandomProvider> mayBeProviderSecureRandom = getRandomProvider(mayBeProviderSecureRandomArg, x -> new SecureRandomProvider());
        Optional<RandomProvider> mayBeProviderSystemIn = getRandomProvider(mayBeProviderSystemInArg, x -> new SystemInProvider());
        Optional<RandomProvider> firstProvider = getFirstProvider(mayBeProviderRandomOrg, mayBeProviderSecureRandom, mayBeProviderSystemIn);

        return firstProvider;
    }

    private Optional<Integer> getValidatedSampleSize(Optional<OptionSet> mayBeOptionSet) {
        Optional<Integer> mayBeRandomStringLength = getRandomStringLength(mayBeOptionSet);
        Optional<Boolean> mayBeProviderSystemInArg = getSystemInProviderArg(mayBeOptionSet);
        Optional<Integer> mayBeSampleSize = getSampleSize(mayBeOptionSet);
        Optional<Boolean> validateSampleSize = getValidSampleSize(mayBeProviderSystemInArg, mayBeSampleSize, mayBeRandomStringLength);

        checkOptional(validateSampleSize, "sample-size should not be greater that random-string-length");

        return mayBeSampleSize;
    }


    void printHelpAndExit() {
        printHelp();
        exit(OK);
    }

    private Optional<OptionSet> tryParse(String[] args) {
        try {
            return Optional.of(parser.parse(args));
        } catch (OptionException optionException) {
            return Optional.empty();
        }
    }

    private Optional<Boolean> getValidStringLength(Optional<Boolean> mayBeProviderRandomOrgArg,
                                                   Optional<Integer> mayBeRandomStringLength) {
        return mayBeProviderRandomOrgArg
                .flatMap(x -> mayBeRandomStringLength.map(length -> !x || (length <= MAX_RANDOM_ORG)));
    }


    private Optional<Boolean> getValidSampleSize(Optional<Boolean> mayBeProviderSystemInArg,
                                                 Optional<Integer> mayBeSampleSize,
                                                 Optional<Integer> mayBeRandomStringLength) {
        return mayBeRandomStringLength
                .flatMap(randomString -> mayBeProviderSystemInArg
                        .flatMap(provider -> mayBeSampleSize
                                .map(sampleSize -> provider || sampleSize < randomString)));
    }


    private Optional<Integer> getRandomStringLength(Optional<OptionSet> mayBeOptionSet) {
        return mayBeOptionSet.map(os -> getIntValue(os, "random-string-length"));
    }

    private Optional<Integer> getSampleSize(Optional<OptionSet> mayBeOptionSet) {
        return mayBeOptionSet.map(os -> getIntValue(os, "sample-size"));
    }

    private Optional<RandomProvider> getRandomProvider(Optional<Boolean> mayBeProviderRandomOrgArg,
                                                       Function<Boolean,
                                                               RandomProvider> booleanIRandomProviderFunction) {
        return mayBeProviderRandomOrgArg.filter(Boolean.TRUE::equals).map(booleanIRandomProviderFunction);
    }

    private Optional<Boolean> getSystemInProviderArg(Optional<OptionSet> mayBeOptionSet) {
        return mayBeOptionSet.map(os -> os.has("provider-system-in"));
    }

    private Optional<Boolean> getSecureRandomProviderArg(Optional<OptionSet> mayBeOptionSet) {
        return mayBeOptionSet.map(os -> os.has("provider-secure-random"));
    }

    private Optional<Boolean> getRandomOrgProviderArg(Optional<OptionSet> mayBeOptionSet) {
        return mayBeOptionSet.map(os -> os.has("provider-random-org"));
    }

    private Optional<Boolean> runHelpIfAsked(Optional<OptionSet> mayBeOptionSet) {
        Optional<Boolean> mayBeHelpArg = getHelp(mayBeOptionSet);
        mayBeHelpArg.filter(Boolean.TRUE::equals).ifPresent(help -> printHelpAndExit());
        return mayBeOptionSet.map(os -> os.has("help"));
    }

    private Optional<Boolean> getHelp(Optional<OptionSet> mayBeOptionSet) {
        return mayBeOptionSet.map(os -> os.has("help"));
    }

    private void checkOptional(Optional<Boolean> optionalCheck, String errorMessage) {
        optionalCheck.filter(Boolean.FALSE::equals).ifPresent(isValid -> printAndExit(errorMessage, KO));
    }

    private Optional<RandomProvider> getFirstProvider(Optional<RandomProvider> mayBeProviderRandomOrg,
                                                      Optional<RandomProvider> mayBeProviderSecureRandom,
                                                      Optional<RandomProvider> mayBeProviderSystemIn) {
        return Stream.of(
                mayBeProviderRandomOrg,
                mayBeProviderSecureRandom,
                mayBeProviderSystemIn)
                .filter(Optional::isPresent)
                .map(Optional::get).findFirst();
    }

    private void printHelp() {
        try {
            parser.printHelpOn(System.out);
        } catch (IOException e) {
            throw new RuntimeException("error talking with parser", e);
        }
    }

    private void print(String text) {
        System.out.println(text);
    }

    private void exit(int status) {
        System.exit(status);
    }

    private void printAndExit(String text, int code) {
        print(text);
        exit(code);
    }

    private OptionParser getParser() {
        final OptionParser parser = new OptionParser();
        parser.accepts("sample-size", "Sample size").withRequiredArg().required().ofType(Integer.class);

        OptionSpecBuilder providerRandomOrg = parser.accepts("provider-random-org", "Generates random string via random.org client. Required if no other provider selected");
        OptionSpecBuilder providerSecureRandom = parser.accepts("provider-secure-random", "Generates random string based on java.security.SecureRandom. Required if no other provider selected");
        OptionSpecBuilder providerSystemIn = parser.accepts("provider-system-in", "Using passed stream as random string. Required if no other provider selected");

        providerRandomOrg
                .availableUnless("provider-secure-random", "provider-system-in")
                .requiredUnless("provider-secure-random", "provider-system-in");

        providerSecureRandom
                .availableUnless("provider-random-org", "provider-system-in")
                .requiredUnless("provider-random-org", "provider-system-in");

        providerSystemIn
                .availableUnless("provider-random-org", "provider-secure-random")
                .requiredUnless("provider-random-org", "provider-secure-random");

        parser.accepts("random-string-length", "Random string length. Required if one of --provider-random-org, --provider-secure-random providers is enabled ")
                .requiredIf("provider-random-org", "provider-secure-random")
                .withOptionalArg()
                .ofType(Integer.class)
                .defaultsTo(20);

        parser.accepts("help", "Print help information").forHelp();
        return parser;
    }


    private Integer getIntValue(OptionSet options, String option) {
        return (Integer) options.valueOf(option);
    }
}
