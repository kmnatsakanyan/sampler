package de.researchgate.sample;

import java.security.SecureRandom;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ReservoirSampler<T> implements Collector<T, List<T>, List<T>> {

    private final Random rand = new SecureRandom();
    private final int sampleSize;
    private int count = 0;

    public ReservoirSampler(int size) {
        this.sampleSize = size;
    }

    private void sample(final List<T> list, T s) {
        if (list.size() < sampleSize) {
            list.add(s);
        } else {
            int replaceInIndex = (int) (rand.nextDouble() * (sampleSize + (count++) + 1));
            if (replaceInIndex < sampleSize) {
                list.set(replaceInIndex, s);
            }
        }
    }

    @Override
    public Supplier<List<T>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<T>, T> accumulator() {
        return this::sample;
    }

    @Override
    public BinaryOperator<List<T>> combiner() {
        return (left, right) -> {
            left.addAll(right);
            return left;
        };
    }

    @Override
    public Set<java.util.stream.Collector.Characteristics> characteristics() {
        return EnumSet.of(Collector.Characteristics.UNORDERED, Collector.Characteristics.IDENTITY_FINISH);
    }

    @Override
    public Function<List<T>, List<T>> finisher() {
        return (i) -> i;
    }

}

