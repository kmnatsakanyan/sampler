package de.researchgate.app;

import de.researchgate.random.orgrandom.RandomOrgProvider;
import de.researchgate.random.secure.SecureRandomProvider;
import de.researchgate.random.systemin.SystemInProvider;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CommandLineToolsTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Spy
    private CommandLineTools commandLineTools;

    @Test
    public void testTryCreateSampler_randomOrgProvider() throws Exception {
        String[] args = new String[]{"--provider-random-org", "--sample-size", "5", "--random-string-length", "10"};
        Optional<Sampler> sampler = commandLineTools.tryCreateSampler(args);
        assertTrue(sampler.isPresent());
        assertEquals(RandomOrgProvider.class, sampler.get().getRandomProvider().getClass());
        assertEquals(10, sampler.get().getRandomStringLength().intValue());
        assertEquals(5, sampler.get().getSampleSize().intValue());
        List<Character> result = sampler.get().start();
        assertEquals(5, result.size());
    }

    @Test
    public void testTryCreateSampler_randomOrgProvider_invalidRandomStringLength() throws Exception {
        exit.expectSystemExit();
        String[] args = new String[]{"--provider-random-org", "--sample-size", "5", "--random-string-length", "21"};
        commandLineTools.tryCreateSampler(args);
    }

    @Test
    public void testTryCreateSampler_randomOrgProvider_invalidSampleSize() throws Exception {
        exit.expectSystemExit();
        String[] args = new String[]{"--provider-random-org", "--sample-size", "5", "--random-string-length", "4"};
        commandLineTools.tryCreateSampler(args);
    }

    @Test
    public void testTryCreateSampler_systemInProvider() throws Exception {
        String[] args = new String[]{"--provider-system-in", "--sample-size", "15", "--random-string-length", "100"};
        Optional<Sampler> sampler = commandLineTools.tryCreateSampler(args);
        assertTrue(sampler.isPresent());
        assertEquals(SystemInProvider.class, sampler.get().getRandomProvider().getClass());
        assertEquals(100, sampler.get().getRandomStringLength().intValue());
        assertEquals(15, sampler.get().getSampleSize().intValue());
    }

    @Test
    public void testTryCreateSampler_secureRandomProvider() throws Exception {
        String[] args = new String[]{"--provider-secure-random", "--sample-size", "15", "--random-string-length", "100"};
        Optional<Sampler> sampler = commandLineTools.tryCreateSampler(args);
        assertTrue(sampler.isPresent());
        assertEquals(SecureRandomProvider.class, sampler.get().getRandomProvider().getClass());
        assertEquals(100, sampler.get().getRandomStringLength().intValue());
        assertEquals(15, sampler.get().getSampleSize().intValue());
        List<Character> result = sampler.get().start();
        assertEquals(15, result.size());
    }

    @Test
    public void testTryCreateSampler_help() throws Exception {
        exit.expectSystemExit();
        String[] args = new String[]{"--help"};
        Optional<Sampler> sampler = commandLineTools.tryCreateSampler(args);
        assertFalse(sampler.isPresent());
    }

    @Test
    public void testPrintResult() throws Exception {
        List<Character> listToPrint = "RANDOMSTRING".chars()
                .mapToObj(i -> (char) i)
                .collect(Collectors.toList());
        commandLineTools.printResult(listToPrint);
        assertEquals("########## Sample ##########\nRANDOMSTRING\n", systemOutRule.getLog());
    }

}