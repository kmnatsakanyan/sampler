package de.researchgate.random.orgrandom;


import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class PrimitiveConverterFactory extends Converter.Factory {

    private PrimitiveConverterFactory() {
    }

    public static PrimitiveConverterFactory create() {
        return new PrimitiveConverterFactory();
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        if (type == String.class) {
            return new Converter<ResponseBody, String>() {
                @Override
                public String convert(ResponseBody value) throws IOException {
                    return value.string().replaceAll("\n", "");
                }
            };
        } else if (type == Integer.class) {
            return new Converter<ResponseBody, Integer>() {
                @Override
                public Integer convert(ResponseBody value) throws IOException {
                    return Integer.valueOf(value.string().replaceAll("\n", ""));
                }
            };
        } else if (type == Double.class) {
            return new Converter<ResponseBody, Double>() {
                @Override
                public Double convert(ResponseBody value) throws IOException {
                    return Double.valueOf(value.string());
                }
            };
        }
        // or null if type cannot be handled by this factory
        return null;
    }
}
