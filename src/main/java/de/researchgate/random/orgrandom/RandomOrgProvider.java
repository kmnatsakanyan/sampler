package de.researchgate.random.orgrandom;

import de.researchgate.app.AppConfig;
import de.researchgate.random.RandomProvider;

import java.io.IOException;
import java.util.stream.Stream;


public class RandomOrgProvider implements RandomProvider {

    private String getRandomString(int max) {
        try {
            return AppConfig
                    .getRandomApiClient()
                    .getRandomString(1, max, "on", "on", "on", "on", "plain")
                    .execute()
                    .body();
        } catch (IOException e) {
            throw new RuntimeException("Exception while getting random string form random.org", e);
        }
    }

    @Override
    public Stream<Character> getRandomCharStream(Integer max) {
        String randomString = getRandomString(max);
        printRandomString(randomString);
        return randomString.chars()
                .mapToObj(i -> (char) i);
    }


}
