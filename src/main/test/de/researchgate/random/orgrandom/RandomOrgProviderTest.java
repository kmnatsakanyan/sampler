package de.researchgate.random.orgrandom;

import de.researchgate.random.RandomProvider;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class RandomOrgProviderTest {

    @Test
    public void testGetRandomCharList() throws Exception {
        RandomProvider secureRandomProvider = new RandomOrgProvider();

        List<Character> firstRandomCharList = secureRandomProvider.getRandomCharStream(20).collect(Collectors.toList());
        assertEquals(20, firstRandomCharList.size());
    }

    @Test(expected = NullPointerException.class)
    public void testGetRandomCharList_throwsException() throws Exception {
        RandomProvider secureRandomProvider = new RandomOrgProvider();

        secureRandomProvider.getRandomCharStream(21);
    }
}