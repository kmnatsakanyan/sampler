package de.researchgate.app;

import de.researchgate.random.RandomProvider;
import de.researchgate.sample.ReservoirSampler;

import java.util.List;

public class Sampler {

    private Integer sampleSize;
    private Integer randomStringLength;
    private RandomProvider randomProvider;

    public Sampler(Integer sampleSize, Integer randomStringLength, RandomProvider randomProvider) {
        this.sampleSize = sampleSize;
        this.randomStringLength = randomStringLength;
        this.randomProvider = randomProvider;
    }

    public List<Character> start() {
        return randomProvider
                .getRandomCharStream(randomStringLength)
                .collect(new ReservoirSampler<>(sampleSize));
    }

    public Integer getSampleSize() {
        return sampleSize;
    }

    public Integer getRandomStringLength() {
        return randomStringLength;
    }

    public RandomProvider getRandomProvider() {
        return randomProvider;
    }

}
