package de.researchgate.app;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class App {

    private static CommandLineTools commandLineTools = new CommandLineTools();

    public static void main(String[] args) throws IOException {

        Optional<Sampler> sampler = commandLineTools.tryCreateSampler(args);

        if (sampler.isPresent()) {
            List<Character> result = sampler.get().start();
            commandLineTools.printResult(result);
            printMemoryUsage();
        } else {
            commandLineTools.printHelpAndExit();
        }
    }

    private static void printMemoryUsage() {
        Runtime runtime = Runtime.getRuntime();
        int mb = 1024 * 1024;
        System.out.println("Used Memory: " + (runtime.totalMemory() - runtime.freeMemory()) / mb + "MB");
    }

}
