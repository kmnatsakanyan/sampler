package de.researchgate.random.orgrandom;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RandomApiClient {

    @GET("integers")
    Call<Integer> getRandomNumber(@Query("num") int num,
                                  @Query("min") int min,
                                  @Query("max") int max,
                                  @Query("col") int col,
                                  @Query("base") int base,
                                  @Query("format") String format);


    @GET("strings")
    Call<String> getRandomString(@Query("num") int num,
                                 @Query("len") int len,
                                 @Query("digits") String enableDigits,
                                 @Query("upperalpha") String enableupperalpha,
                                 @Query("loweralpha") String enableLoweralpha,
                                 @Query("unique") String enableUnique,
                                 @Query("format") String format);
}
